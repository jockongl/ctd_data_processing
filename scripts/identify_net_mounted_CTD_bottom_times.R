################################################################################################
##
## Script name: identify_netCTD_bottom_times.R
##
## Purpose: 
## code to open and explore/process '.asc' CTD text (tab-separated) files from a net-mounted
## CTD device; the script tries to identify periods when the net was on the bottom, based on 
## the descent rate (and plots of descent rate and depth); the 'net landing' and 'net take-off'
## indices are then saved to a table and plots are made of all the depth and descent rate
## profiles. Using those plots, the identified landing and take-off times (indices) can be 
## manually adjusted for those stations where the automated identification did not work well.
## after the visual check and manual adjustment of indices, the next script 
## (extract_average_CTD_variabls.R) can be run to extract the bottom variables.
##
## Note: the 2014 Andromeda files processed by Grant probably need to be re-processed; they 
## contain duplicate fields of Sal00	OxsolML/L	and OxsatML/L, with slightly different values,
## so uncertain which are the 'correct' or 'better' data

##
## Author: Jock Currie
##
## Date created: 1 March 2021
## 
####################
## notes from slack (Donia): There should be 1 file per trawl (although I have seen a file labelled as two trawls…). The naming of the file should be in the format ST#####_TR### (station and trawl number)
##  I have left the variable names of the column labels as is outputted; in case the labels aren’t clear, they represent in order, Temperature (C), Conductivity (S/m), Pressure (db), Depth (m), Oxygen raw (V), Turbidity (NTU), PAR/Irradiance, Fluorescence (mg/m^3), Descent rate (m/s), Time elapsed (sec), Scan count, Voltage 0-3, Salinity (PSU), Oxygen saturation, Garcia & Gordon (ml/L), Oxygen saturation, Weiss (ml/L), Flag.
##
## Note: before running, A33884 CTD ascii file contains two trawl profiles - the CTD was obviously not switched off between trawls, so it needs to be manually cut and saved into two files, the first half should be A33884 and the second half will be A33885; currently the latter is automatically being identified as the profile! When reviewing profiles of all data, check for such issues. It is important to visually check every single CTD profile (at least the descent rate profile and if anything looks unusual there, then also the corresponding depth profile)
## Also, D00232-236_TR108-112AND002_2014_WC_DC_Filter_AlignTO_CTM_Derive_ASCII.asc consists of 5 complete CTD profiles (and thereafter a bunch of rubbish): chuck everything beyond 17700 seconds and cut the parts before that up into the 5 stations, which I do below, but this should only be done once:
## NB: RUN THIS ONLY ONCE and thereafter comment it out:
# flnms <- list.files(path = "data_raw", pattern = "D00232-236_TR108-112AND002_2014_.*.asc$", recursive = T, full.names = T)
# dta <- read.delim(flnms)
# dim(dta)
# plot(dta$TimeS, -dta$DepSM)
# NperSec <- round(length(dta$TimeS)/diff(range(dta$TimeS, na.rm=T))) 
# dta <- dta[1:(17750*NperSec),]
# plot(dta$TimeS, -dta$DepSM)
# dta1 <- dta[1:(3900*NperSec),]
# plot(dta1$TimeS, -dta1$DepSM)
# dta2 <- dta[(3900*NperSec):(7200*NperSec),]
# plot(dta2$TimeS, -dta2$DepSM)
# dta3 <- dta[(7200*NperSec):(10800*NperSec),]
# plot(dta3$TimeS, -dta3$DepSM)
# dta4 <- dta[(10800*NperSec):(14200*NperSec),]
# plot(dta4$TimeS, -dta4$DepSM)
# dta5 <- dta[(14200*NperSec):nrow(dta),]
# plot(dta5$TimeS, -dta5$DepSM)
# write.table(dta1, sep = '\t', row.names = F, quote = F, file = sub(pattern = "D00232-236_TR108-112AND002_2014_.*.asc$", replacement = "D00232_TR108AND002_2014_WC_DC_Filter_AlignTO_CTM_Derive_ASCII.asc", flnms)) # this will not re-insert the original '/' in the filenames, but if used in R and if 'check.names' is left as T on read.delim, then the read data will be consistent with other files.
# write.table(dta2, sep = '\t', row.names = F, quote = F, file = sub(pattern = "D00232-236_TR108-112AND002_2014_.*.asc$", replacement = "D00233_TR109AND002_2014_WC_DC_Filter_AlignTO_CTM_Derive_ASCII.asc", flnms)) 
# write.table(dta3, sep = '\t', row.names = F, quote = F, file = sub(pattern = "D00232-236_TR108-112AND002_2014_.*.asc$", replacement = "D00234_TR110AND002_2014_WC_DC_Filter_AlignTO_CTM_Derive_ASCII.asc", flnms)) 
# write.table(dta4, sep = '\t', row.names = F, quote = F, file = sub(pattern = "D00232-236_TR108-112AND002_2014_.*.asc$", replacement = "D00235_TR111AND002_2014_WC_DC_Filter_AlignTO_CTM_Derive_ASCII.asc", flnms)) 
# write.table(dta5, sep = '\t', row.names = F, quote = F, file = sub(pattern = "D00232-236_TR108-112AND002_2014_.*.asc$", replacement = "D00236_TR112AND002_2014_WC_DC_Filter_AlignTO_CTM_Derive_ASCII.asc", flnms))
# file.rename(from = flnms, to=paste0(flnms,"_backup"))
##
####################################################################################################
### settings

## station or trawl log data
stnfle <- "data_raw/trawl_cleaned.csv"

## descent thresholds for identifying start and end points of 'bottom time'
# (following application of a moving average smoothing, a minimum threshold to use either side of 0 to identify when the net is ascending or descending; hopefully large enough to not be triggered by events/net movement during the trawl but identify the points in the descent/ascent just before landing & just after take-off; made the ascent threshold more sensitive to pick up the hauling 'blip'; check the 'net_on_bottom_checking_descentRate' and 'net_on_bottom_checking_depth' pdf files for plots that help explain this approach
strt_thresh <- 0.17
end_thresh <- 0.05

ordr <- 59 # select the number of seconds to calculate moving average (of depth or descent rate) by; Note: this is also used below to pad half those number of seconds 'outwards' to estimate the true landing/take-off time.

## packages
# require(here)
require(dplyr)

####################################################################################################
### load station data
stndta <- read.csv(stnfle, stringsAsFactors = F)
head(stndta)
## check whether 'ST33903' from CTD files corresponds to station number A33903?
indx <- which(stndta$station == "A33903")
stndta[indx,] 
## this uncovered an apparent error - the station data for A33903 looks to be from March 2019, not January 2019 (as indicated in the CTD filenames)?

####################################################################################################
### get a list of all .asc files in data_raw folder and cycle through them one by one, plotting their descent rate profiles

flnms <- list.files(path="data_raw/dffe_ctd/donia_extracted", pattern = "derive_ascii.asc$", full.names = T, recursive = TRUE) # get a vector of all the filenames extracted by Donia and that match pattern '.asc'
## add the 2019 AFR297 data that jordan extracted:
flnms <- c(flnms, list.files(path="data_raw/dffe_ctd/jordan_extracted/2019_AFR297_SC_19plus", pattern = "Out.asc$", full.names = T, recursive = TRUE))
## add the 2014 data processed by Garth; bizarrely the 2014 data are netmounted, whereas the 2016 data were rosette CTD
flnms <- c(flnms, list.files(path="data_raw/dffe_ctd/grant_extracted", pattern = "ASCII.asc$", full.names = T, recursive = TRUE))
length(flnms) # how many are there
# flnms <- flnms[1:20] # reduce to first 20 for script testing purposes

# ## if want to exclude specific files, do so by including patterns from their filenames in the pattern argument below (but be sure those patterns aren't repeated in other files you don't want to remove!) e.g.:
# flnms <- flnms[grep(pattern = "TR112|TR115", flnms, invert=T)]

## checking CTD filenames to devise string patterns to differentiate between net-mounted and rosette CTD data
# 2011-2016 are rosette CTD data, thereafter is net-mounted
# 2011:
#   Stn002_2011_WC_DC_Filter_AlignCTD_CTM_LoopEdit_Derive_ASCII_Out.asc # jordan LoopEdit
#   Stn002_2011_SC_DC_Filter_AlignCTD_CTM_LoopEdit_Derive_ASCII_Out.asc # jordan
# 2012:
#   stn002_2012_WC_DC_Filter_AlignCTD_CTM_LoopEdit_Derive_ASCII_Out.asc # jordan
# 2014:
#   D00179_TR055AND002_2014_WC_DC_Filter_AlignTO_CTM_Derive_ASCII.asc # grant 
# 2016:
#   stn002_2016_SC_DC_Filter_AlignCTD_CTM_LoopEdit_Derive_ASCII_Out.asc # jordan
# 2017:
#   ST33061_TR002_2017_WC_DC_Filter_AlignTO_CTM_Derive_ASCII.asc # jordan
#   ST33061_TR002_2017_AFR291_DC_filter_align_CTM_derive_ascii.asc # donia
# 2019:
#   ST33785_TR002_2019_WC_DC_Filter_AlignTO_CTM_Derive_ASCII Out.asc # jordan
#   ST33785_TR002_2019_AFR296_DC_filter_align_CTM_derive_ascii.asc  # donia 
#   ST33908_TR002_2019_SC_DC_Filter_AlignTO_CTM_Derive_ASCII Out.asc # jordan
# 2020:
#   ST34443_TR002_2020_WC_DC_Filter_AlignTO_CTM_Derive_ASCII Out.asc # jordan
#   ST34443_TR002_2020_AFR300_DC_filter_align_CTM_derive_ascii.asc # donia


## create a 'lookup' table to match existing variables and (new) names to, as not all files will have the same sequence (or number of) variables; hopefully they're named the same!
varsOfinterest <- as.data.frame(cbind(
  origVars = c("Tv290C", "C0S.m", "PrdM", "DepSM", "Sbeox0V", "Sbeox0ML.L", "TurbWETntu0", "Par", "FlECO.AFL", "Dz.dtM", "TimeS", "Scan", "Sal00", "Sal00.1", "OxsolML.L", "OxsolML.L.1", "OxsatML.L", "OxsatML.L.1", "N2satML.L", "Flag"),
  newNames = c("Temperature", "Conductivity", "Pressure", "Depth", "Oxygen_raw", "Oxygen_sensor", "Turbidity", "PAR", "Fluorescence", "DescentRate", "Time_seconds", "Scan_ID", "Salinity", "Salinity1", "OxygenSat_Garcia_Gordon", "OxygenSat_Garcia_Gordon1", "OxygenSat_Weiss", "OxygenSat_Weiss1", "N2Sat", "Flag")))
save(varsOfinterest, file="lookup_CTDvars.RData")

# oxygen from sensor is Sbeox0ML.L


## save the landing and liftoff indices in a table for later use:
tbl_landlift <- data.frame(cbind(station=rep(NA, length(flnms)),
                                 indx_strt=NA,
                                 indx_end=NA))

pdf("plots/net_on_bottom_checking_descentRate.pdf", paper="A4", pointsize = 12)
# plt.count <- 0
rws <- 5
par(mfrow=c(rws,3))# change plottign window to 2 rows, one column
par(mar=c(4,2,0,0)) # margins c(bottom, left, top, right) the default is c(5, 4, 4, 2) + 0.1.
indx_rw <- 0
for (nm in flnms) { # nm = flnms[476] #
  
  ## print filename
  print(paste0("plotting ", nm))
  
  indx_rw <- indx_rw + 1
  
  ## extract the station code/number
  ## this will probably have to be adjusted to handle the compass challenger and Andromeda stations, which have a different code
  if (grepl("grant", nm))
    stn <- sub("_TR.*", "", tail(unlist(strsplit(nm, "/")),1)) else # stn <- sub("ST", "A", sub("_TR.*", "", tail(unlist(strsplit(nm, "/")),1)))  could probably catch both cases
      stn <- sub(".*/ST", "A", sub("_TR.*", "", nm)) # use sub to replace the parts of the filename to reduce it to the station number and add 'A' instead of the 'ST'; probably a better way of doing this than with one 'sub' function inside another?
  
  tbl_landlift$station[indx_rw] <- stn
    
  # nm <- "data_raw/ST33903_TR120_Jan_2019_DC_Filter_AlignTO_CTM_Derive.asc"
  dta <- read.delim(nm) # read data
  dim(dta) # dimensions of table
  head(dta) # check the first few lines
  
  # not necessary after changing select and renaming below ## drop voltage columns - of not interest/use?
  # dta <- select(dta, -any_of(c("V0", "V1", "V2", "V3", "V323"))) # use 'any_of' to avoid error if one of these columns doesn't exist;

  ## for Jordan's CTD data; drop one of the two (unwanted) oxygen columns:
  if (length(grep("Sbeox0ML.L", colnames(dta))) > 1) { # if there is more than one column named Sbeox0ML.L 
    dta <- dta[,-(grep("Sbeox0ML.L", names(dta))[2])] # remove the first (select index 1) or change to [2] to remove the second one
  }
    
  ## select variables and rename columns for easier understanding & use
  dta <- dta[,grep(paste(varsOfinterest$origVars, collapse = "|"), names(dta))] # dta[,match(varsOfinterest$origVars, names(dta))] # select columns of interest
  names(dta) <- varsOfinterest$newNames[match(names(dta), varsOfinterest$origVars)] # rename the variables using the lookup table, after having dropped the unwanted columns
  head(dta) # check the first few lines
  
  ## does depth return to zero after reaching max depth (> 20m), i.e. is there a full profile? if not, try to extract a 'partial bottom' period
  maxdepth <- max(dta$Depth, na.rm=T)
  indx.maxdepth <- max(which(dta$Depth == maxdepth))
  
  NperSec <- round(length(dta$Time_seconds)/diff(range(dta$Time_seconds, na.rm=T))) # also need to calculate how many measures per second (in case this changes among surveys or files)
  
  ### before carrying on, check that the CTD station has at least 10 minutes of data!
  if (nrow(dta) > 10*60*NperSec) {
  
    ## add moving average for the descent rate
    dta$descentMA <- forecast::ma(dta$DescentRate, order=ordr) # 59 seconds centred moving average #  
    indx.b4strt <- max(which(dta$descentMA > strt_thresh))+20 # this gets re-calculated below, but need it here for the 'else' test below for the uncompleted profiles (where the battery ran out)
    
    if (!is.finite(indx.b4strt)) # is indx.b4strt infinite (i.e. never goes beyond strt_thresh); need to catch, otherwise throws error further down
    indx.b4strt <- 1 # include entire profile
  
  if (maxdepth > 20 & min(dta$Depth[indx.maxdepth:nrow(dta)]) < 0.5) { # is the profile deeper than 20m and does the profile return to <0.5 m after reaching its maximum
    
    ## identify indices that identify the points in time just before landing and just after take-off, using the descent rate (with moving average applied)
    indx.b4strt <- max(which(dta$descentMA > strt_thresh)) # the first point at which descentMA goes beyond thresh
    indx.aftend <- min(which(dta$descentMA < -end_thresh)) # the first point where descentMA goes beyond -thresh
    # indx.b4strt <- max(which(dta$descentMA == max(dta$descentMA, na.rm = T))) # the first point at which descentMA goes beyond thresh
    # indx.aftend <- min(which(dta$descentMA == min(dta$descentMA, na.rm = T))) # the first point where descentMA goes beyond -thresh
    
    if (indx.aftend < indx.b4strt) # if the end point is before the start point, try to find a second end point, ignoring the first
      indx.aftend <- indx.b4strt + min(which(dta$descentMA[indx.b4strt:nrow(dta)] < -end_thresh))
    
    ## select the five minutes after (strt) and before (end), from which periods the first and last crossing of zero descent rate should identify the landing & lift-off of the nedt
    indx.strt.period <- indx.b4strt:(indx.b4strt+10*NperSec*60) # indices of the start plus 10 minutes
    indx.end.period <- (indx.aftend-5*NperSec*60):indx.aftend # indices of the 'end' minus 5 minutes
    
    ## identify the start and end points
    indx.trwlStrt <- indx.b4strt+min(which(dta$descentMA[indx.strt.period] < 0.01)) - floor(ordr/2) # within the start period (indices), which one is the first to cross to a negative descentMA rate, i.e. when does the net stop descending and then add the padding of half the moving average 'order'; although in theory this would be 'crossing zero', if the net lands on a decline (e.g. A33804) and keeps descending even after landing on the seafloor, then 0 does not trigger the 'landing' so I've updated it to < 0.01
    if (is.infinite(indx.trwlStrt)) # few cases (e.g. A34446) where lands on a steeper decline and start trawl requires more lenient threshold:
      indx.trwlStrt <- indx.b4strt+min(which(dta$descentMA[indx.strt.period] < 0.03)) - floor(ordr/2) 
    
    indx.trwlEnd <- indx.aftend - length(indx.end.period) + max(which(dta$descentMA[indx.end.period] > 0)) + floor(ordr/2) # within the end period (indices), which one is the last one where descentMA is positive, i.e. before it starts lifting off and then add the padding of half the moving average 'order'
    if (is.infinite(indx.trwlEnd)) # few cases (e.g. A34446) where lands on a steeper decline and start trawl requires more lenient threshold:
      indx.trwlEnd <- indx.aftend - length(indx.end.period) + max(which(dta$descentMA[indx.end.period] > -0.03)) + floor(ordr/2)
    
    ## if the bottom period is longer than 35 minutes, there is likely a post-haul bottom time, so then reduce to 30 minutes:
    if (dta$Time_seconds[indx.trwlEnd]/60 - dta$Time_seconds[indx.trwlStrt]/60 > 35)
      indx.trwlEnd <- indx.trwlStrt + 30*60*NperSec
    
    ## if the bottom period is shorter than 19 minutes, then lengthen it to 20 minutes after start. this occurred when the net (ground?) was very jumpy, or when it was dragged up an incline; can't extend it longer than 20 minutes, as some trawls were curtailed to 20 minutes and one would risk including the water-column data; it is better to chop off some of the real bottom time than to include measurements way off the seafloor; however, do this extension only if the profile extends more than 20 minutes (otherwise you end up with a non-existant end index that is beyond the end of the profile, which can happen in an abandoned trawl, e.g. A33814)
    if (dta$Time_seconds[indx.trwlEnd]/60 - dta$Time_seconds[indx.trwlStrt]/60 < 19 & nrow(dta) > indx.trwlStrt+19*60*NperSec) #
      indx.trwlEnd <- indx.trwlStrt + 20*60*NperSec
    
    ## save the start and end indx in the tbl_landlift table for later use:
    tbl_landlift$indx_strt[indx_rw] <- indx.trwlStrt
    ## save an end indx only if the period between the start and end indices is 19 min or longer (to avoid having a bottom time for aborted trawls, e.g. A33814) # this will help catch those aborted trawls in the following script, i.e. if there is only a start and not an end index, do not calculate any bottom variables.
    if (length(indx.trwlStrt:indx.trwlEnd) >= 19*60*NperSec)
      tbl_landlift$indx_end[indx_rw] <- indx.trwlEnd
    
    ## plot for visual checking
    # plt.count <- plt.count + 1
    
    ## plot descent rate vs time profile, together with start and end points etc
    plot(dta$Time_seconds/60, dta$descentMA, pch=20, cex=0.5, xlab="minutes", type="l") #, xlim=c(5,63)) # convert to minutes on x-axis and remove much of the surface intervals
    abline(v=c(dta$Time_seconds[indx.trwlStrt]/60, dta$Time_seconds[indx.trwlEnd]/60), col="red")
    abline(h=c(strt_thresh), col="red", lty=2)
    abline(h=c(-end_thresh), col="red", lty=2)
    # abline(h=0, col="blue", lty=2)
    text(x=0.04*max(dta$Time_seconds/60), y=0.5*min(dta$descentMA, na.rm = T), labels = stn, pos=4, cex=1.5)
    text(x=0.04*max(dta$Time_seconds/60), y=0.8*min(dta$descentMA, na.rm = T), labels = sub("data_raw/", "", nm), pos=4, cex=0.8) # add
    text(x=0.64*max(dta$Time_seconds/60), y=0.8*max(dta$descentMA, na.rm = T), labels = paste(round(dta$Time_seconds[indx.trwlEnd]/60 - dta$Time_seconds[indx.trwlStrt]/60, 0.9), "minutes"), pos=4, cex=0.9) # add the number of minutes between start and end points in the top-right corner
  } else 
    if (maxdepth > 20 & max(abs(range(dta$descentMA[indx.b4strt:nrow(dta)], na.rm=T))) < 0.17 & length(indx.b4strt:nrow(dta)) > 2*NperSec) { # from the previous 'if' statement, the profile does not come back up to surface (or is less than 20m depth); so now check a) if it goes beyond 20m, b) if it stops descending and remains at relatively constant depth for the remainder of its profile (within a narrow descent rate range) and c) whether that remainder of its 'constant depth' profile is longer than 2 minutes (i.e. it measures at least 2 minutes of bottom variables before the battery dies); the descent rate threshold was set after looking at 'bumpy' profiles, e.g. A33811 is very bumpy (up to 0.153)
      
      ## estimate a start index and the last data point will be the end index
      indx.b4strt <- max(which(dta$descentMA > strt_thresh)) # the first point at which descentMA goes beyond thresh
      ## select the five minutes after (strt)
      NperSec <- round(length(dta$Time_seconds)/max(dta$Time_seconds, na.rm=T)) # first need to calculate how many measures per second:
      indx.strt.period <- indx.b4strt:(indx.b4strt+10*NperSec*60) # indices of the start plus 10 minutes
      ## identify the start point
      indx.trwlStrt <- indx.b4strt+min(which(dta$descentMA[indx.strt.period] < 0.01)) - floor(ordr/2) # within the start period (indices), which one is the first to cross to a negative descentMA rate, i.e. when does the net stop descending and then add the padding of half the moving average 'order'; although in theory this would be 'crossing zero', if the net lands on a decline (e.g. A33804) and keeps descending even after landing on the seafloor, then 0 does not trigger the 'landing' so I've updated it to < 0.01
      indx.trwlEnd <- nrow(dta)
      
      ## save the start and end indx in the tbl_landlift table for later use:
      tbl_landlift$indx_strt[indx_rw] <- indx.trwlStrt
      tbl_landlift$indx_end[indx_rw] <- indx.trwlEnd
      
      ## plot descent rate vs time profile, together with start and end points etc
      plot(dta$Time_seconds/60, dta$descentMA, pch=20, cex=0.5, xlab="minutes", type="l") #, xlim=c(5,63)) # convert to minutes on x-axis and remove much of the surface intervals
      abline(v=c(dta$Time_seconds[indx.trwlStrt]/60, dta$Time_seconds[indx.trwlEnd]/60), col="red")
      abline(h=c(strt_thresh), col="red", lty=2)
      abline(h=c(-end_thresh), col="red", lty=2)
      # abline(h=0, col="blue", lty=2)
      text(x=0.5*max(dta$Time_seconds/60), y=0.4*max(dta$descentMA, na.rm = T), labels = stn, pos=4, cex=1.5)
      text(x=0.2*max(dta$Time_seconds/60), y=0.2*max(dta$descentMA, na.rm = T), labels = sub("data_raw/", "", nm), pos=4, cex=0.8, col="red") # add
      text(x=0.64*max(dta$Time_seconds/60), y=0.8*max(dta$descentMA, na.rm = T), labels = paste(round(dta$Time_seconds[indx.trwlEnd]/60 - dta$Time_seconds[indx.trwlStrt]/60, 0.9), "minutes"), pos=4, cex=0.9) # add the number of minutes between start and end points in the top-right corner
      
    } else { # otherwise simply plot the profile and don't save any data - probably an aborted or problematic station; but check these in case a manual intervention can still extract true bottom data!
      print(paste("profile for station:
                ", nm, "
                seems incomplete, CHECK"))
      # plt.count <- plt.count + 1
      ## plot depth vs time, but select only the parts that represent the trawl:
      plot(dta$Time_seconds/60, dta$descentMA, pch=20, cex=0.5, xlab="minutes", col="red", type="l") #, xlim=c(5,63)) # convert to minutes on x-axis and remove much of the surface intervals
      text(x=0.04*max(dta$Time_seconds/60), y=-0.5*max(dta$descentMA, na.rm = T), labels = stn, pos=4, cex=1.5)
      text(x=0.04*max(dta$Time_seconds/60), y=-0.8*max(dta$descentMA, na.rm = T), labels = sub("data_raw/", "", nm), pos=4, cex=0.8) # add the complete filename below the station code
    }
  
  # if (plt.count == 3*rws+1) { # fit 15 plots on a page, 3 columns, 5 rows
  #   par(mfrow=c(rws,3)) # new plotting region/page
  #   plt.count <- 1
  # }
  } else {
    print(paste("profile for station:
                ", nm, "
                has too few (<10min) data! CHECK"))
    ## plot depth vs time, but select only the parts that represent the trawl:
    plot(dta$Time_seconds/60, dta$Depth, pch=20, cex=0.5, xlab="minutes", col="red", type="l") #, xlim=c(5,63)) # convert to minutes on x-axis and remove much of the surface intervals
    text(x=0.04*max(dta$Time_seconds/60), y=0.9*max(dta$Depth, na.rm = T), labels = stn, pos=4, cex=1.5)
    text(x=0.04*max(dta$Time_seconds/60), y=0.8*max(dta$Depth, na.rm = T), labels = sub("data_raw/", "", nm), pos=4, cex=0.8) # add the 
      }
}

dev.off()

####################################################################################################
### do the same, cycling through all ASCII files, plotting their depth profiles to help interpretation of the ones where things seem to have gone wrong

# flnms <- flnms[1:20] # reduce to first 20 for script testing purposes

pdf("plots/net_on_bottom_checking_depth.pdf", paper="A4", pointsize = 12)
# plt.count <- 0
rws <- 5
par(mfrow=c(rws,3))# change plottign window to 2 rows, one column
par(mar=c(4,2,0,0)) # margins c(bottom, left, top, right) the default is c(5, 4, 4, 2) + 0.1.
for (nm in flnms) {
  
  ## extract the station code/number
  ## this will probably have to be adjusted to handle the compass challenger and Andromeda stations, which have a different code
  if (grepl("grant", nm))
    stn <- sub("_TR.*", "", tail(unlist(strsplit(nm, "/")),1)) else
      stn <- sub(".*/ST", "A", sub("_TR.*", "", nm)) # use sub to replace the parts of the filename to reduce it to the station number and add 'A' instead of the 'ST'; probably a better way of doing this than with one 'sub' function inside another?
    
  # nm <- "data_raw/ST33903_TR120_Jan_2019_DC_Filter_AlignTO_CTM_Derive.asc"
  dta <- read.delim(nm) # read data
  dim(dta) # dimensions of table
  head(dta) # check the first few lines
  
  ## select variables and rename columns for easier understanding & use
  dta <- dta[,grep(paste(varsOfinterest$origVars, collapse = "|"), names(dta))] # dta[,match(varsOfinterest$origVars, names(dta))] # select columns of interest
  names(dta) <- varsOfinterest$newNames[match(names(dta), varsOfinterest$origVars)] # rename the variables using the lookup table, after having dropped the unwanted columns
  
  ## does depth return to zero after reaching max depth (> 20m), i.e. is there a full profile? if not, plot only the depth profile
  # maxdepth <- max(dta$Depth, na.rm=T)
  # indx.maxdepth <- max(which(dta$Depth == maxdepth))
  # 
  # if (maxdepth > 20 & min(dta$Depth[indx.maxdepth:nrow(dta)]) < 0.5) {
  #   
  #   ## add moving average for the descent rate
  #   dta$descentMA <- forecast::ma(dta$DescentRate, order=ordr) # 59 seconds centred moving average #  
  #   
  #   ## identify indices that identify the points in time just before landing and just after take-off, using the descent rate (with moving average applied)
  #   indx.b4strt <- max(which(dta$descentMA > strt_thresh)) # the first point at which descentMA goes beyond thresh
  #   indx.aftend <- min(which(dta$descentMA < -end_thresh)) # the first point where descentMA goes beyond -thresh
  #   # indx.b4strt <- max(which(dta$descentMA == max(dta$descentMA, na.rm = T))) # the first point at which descentMA goes beyond thresh
  #   # indx.aftend <- min(which(dta$descentMA == min(dta$descentMA, na.rm = T))) # the first point where descentMA goes beyond -thresh
  #   
  #   if (indx.aftend < indx.b4strt) # if the end point is before the start point, try to find a second end point, ignoring the first
  #     indx.aftend <- indx.b4strt + min(which(dta$descentMA[indx.b4strt:nrow(dta)] < -end_thresh))
  #   
  #   ## select the five minutes after (strt) and before (end), from which periods the first and last crossing of zero descent rate should identify the landing & lift-off of the nedt
  #   indx.strt.period <- indx.b4strt:(indx.b4strt+25*60) # indices of the start plus 5 minutes
  #   indx.end.period <- (indx.aftend-25*60):indx.aftend # indices of the 'end' minus 5 minutes
  #   
  #   ## identify the start and end points
  #   indx.trwlStrt <- indx.b4strt+min(which(dta$descentMA[indx.strt.period] < 0)) - floor(ordr/2) # within the start period (indices), which one is the first to cross to a negative descentMA rate, i.e. when does the net stop descending and then add the padding of half the moving average 'order'
  #   indx.trwlEnd <- indx.aftend - length(indx.end.period) + max(which(dta$descentMA[indx.end.period] > 0)) + floor(ordr/2) # within the end period (indices), which one is the last one where descentMA is positive, i.e. before it starts lifting off and then add the padding of half the moving average 'order'
  #   
  
  ## get the previously saved indices for start and end of bottom time:
  indx_rw <- match(stn, tbl_landlift$station)
  if (!is.na(indx_rw) & !is.na(tbl_landlift$indx_strt[indx_rw]) & !is.na(tbl_landlift$indx_end[indx_rw])) { # also check that both start and end indx are not NA
    indx.trwlStrt <- tbl_landlift$indx_strt[indx_rw]
    indx.trwlEnd <- tbl_landlift$indx_end[indx_rw]
    
    ## plot for visual checking
    # plt.count <- plt.count + 1
    
    ## plot depth vs time, but select only the parts that represent the trawl:
    # plot(dta$Time_seconds/60, -dta$Depth, pch=20, cex=0.5, xlab="minutes") #, xlim=c(5,63)) # convert to minutes on x-axis and remove much of the surface intervals
    # abline(v=c(dta$Time_seconds[indx.trwlStrt]/60, dta$Time_seconds[indx.trwlEnd]/60), col="red")
    plot(dta$Time_seconds/60, -dta$Depth, pch=20, cex=0.5, xlab="minutes", type="l") #, xlim=c(5,63)) # convert to minutes on x-axis and remove much of the surface intervals
    abline(v=c(dta$Time_seconds[indx.trwlStrt]/60, dta$Time_seconds[indx.trwlEnd]/60), col="red")
    # abline(h=c(-thresh,thresh), col="red", lty=2)
    text(x=0.04*max(dta$Time_seconds/60), y=-0.5*max(dta$Depth, na.rm = T), labels = stn, pos=4, cex=1.5)
    text(x=0.04*max(dta$Time_seconds/60), y=-0.8*max(dta$Depth, na.rm = T), labels = sub("data_raw/", "", nm), pos=4, cex=0.8) # add 
    text(x=0.5*max(dta$Time_seconds/60), y=-0.2*max(dta$Depth, na.rm = T), labels = paste(round(dta$Time_seconds[indx.trwlEnd]/60 - dta$Time_seconds[indx.trwlStrt]/60, 1), "minutes"), pos=4, cex=0.9) # add the number of minutes between start and end points in the top-right corner
    
  } else {
    print(paste("profile for station:
                ", nm, "
                seems incomplete, CHECK"))
    # plt.count <- plt.count + 1
    ## plot depth vs time, but select only the parts that represent the trawl:
    plot(dta$Time_seconds/60, -dta$Depth, pch=20, cex=0.5, xlab="minutes", col="red") #, xlim=c(5,63)) # convert to minutes on x-axis and remove much of the surface intervals
    text(x=0.04*max(dta$Time_seconds/60), y=-0.5*max(dta$Depth, na.rm = T), labels = stn, pos=4, cex=1.5)
    text(x=0.04*max(dta$Time_seconds/60), y=-0.8*max(dta$Depth, na.rm = T), labels = sub("data_raw/", "", nm), pos=4, cex=0.8) # add the complete filename below the station code
    }
  
  # if (plt.count == 3*rws+1) { # fit 15 plots on a page, 3 columns, 5 rows
  #   par(mfrow=c(rws,3)) # new plotting region/page
  #   plt.count <- 1
  # }
  
}

dev.off()

# check A33823

## save the indices for the few files/examples where we may want to manipulate them manually!
write.csv(tbl_landlift, file = "CTD_trawlStation_start_end_bottom_indices.csv", row.names = F)

####################################################################################################
####################################################################################################
