# CTD data processing

This work was a rushed affair to try and help colleagues extract CTD data from many ASCII files; At the time of pushing to GitHub I have not cleaned up the scripts yet; apologies. This was an attempt to create a separate project of the CTD scripts only, which were previously part of a bigger project. If you find missing scripts, mistakes or problems, or improve the code, please let me know. Thanks

## Getting Started

*file/folder structure:* 
the processed CTD ASCII (.asc) files need to be in a folder (or  linked folder in my case) called 'dffe_ctd' that is within a 'data_raw' folder.
The data_raw folder also contains a trawl station table (in my case trawl_cleaned.csv).
The data_raw folder and the outputs folder are added to .gitignore, as we don't have permission to share these data publicly.
The other folders currently in the project folder (and which you may need to create) include 'plots' and 'scripts'

Any required packages will be listed at the beginning of the scripts

## Authors

Jock Currie

## Version History

this is very much a first beta version

## License

Createive Commons licence CC BY-NC

## Acknowledgments

Thanks to Lara Atkinson and her group of students/staff that helped with discussions and especially with the tedious process of processing the raw CTD data (using the proprietary CTD processing software), including Donia Wozniak, Jordan Van Stavel, Grant van der Heever and others.
Much of my code is informed by solutions on Stack Overflow; in this case I made use of 
https://stackoverflow.com/questions/9756360/split-character-data-into-numbers-and-letters

